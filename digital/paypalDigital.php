<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Paypal Digital</title>
<link href ="digital.css" rel = "stylesheet" type = "text/css" />
<script src="https://www.paypal.com/sdk/js?client-id=AZh4FXStuEI-_Kt0Sh4oOe82CC1xm1PQ1MDylAAmKxAWf-fsfzDXWpbUJrW-NK_gQT9LmdPTu-Bx6C-v"></script>
 
<script>
	
  paypal.Buttons({
    createOrder: function(data, actions) {
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: '3.99'
          }
        }]
      });
    },
	onCancel: function (data) {
    	window.location.assign("http://sdmahoney.com/wdv351_finished/cancel.html");
  	},
    onApprove: function(data, actions) {
      return actions.order.capture().then(function(details) {
        alert('Transaction completed by ' + details.payer.name.given_name);
        // Call your server to save the transaction
        return fetch('/paypal-transaction-complete', {
          method: 'post',
          headers: {
            'content-type': 'application/json'
          },
          body: JSON.stringify({
            orderID: data.orderID
          })
        });
      });
    },
	onApprove: function (data) {
		document.cookie = "approved=true; expires=";
		window.location.assign("http://sdmahoney.com/wdv351_finished/digital/delivery.php");
	}
  }).render('#paypal-button-container');
</script>

</head>

<body>
<h1>Paypal Digital Products</h1>
<div class="float">
<h3>Product Contains:</h3>
<ul>
<li>1 Word Document</li>
<li>1 Image File</li>
<li>1 PDF File</li>
<li>1 ZIP File</li>
<li>1 Video File</li>
</ul>
</div>
<div class="buttons">
<div id="paypal-button-container"></div>
</div>
</body>
</html>