<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Products Page</title>
<script>
	
	function getCookie() {
		
		var cookieArr = document.cookie.split(";");
		
		for(var i = 0; i < cookieArr.length; i++) {
			var cookiePair = cookieArr[i].split("=");
			
			if(name == cookiePair[0].trim()) {
				return decodeURIComponent(cookiePair[1]);
			}
		}
		
	}
	
	function checkCookie() {
		var approveUser = getCookie("approved");
		
		if(approveUser == "true") {
			console.log("Payment Processed.")
		}
		else {
			window.location.assign("http://sdmahoney.com/wdv351_finished/digital/cancel.html");
		}
	}

</script>
<link href ="digital.css" rel = "stylesheet" type = "text/css" />
</head>

<body onload="checkCookie()">
<h1>Thank you for your purchase:</h1>
<h2><a href="products/word_file.docx" download>Word Document</a></h2>
<h2><a href="products/image_file.jpg" download>Image</a></h2>
<h2><a href="products/pdf_file.pdf" download>PDF</a></h2>
<h2><a href="products/zip_file.zip" download>ZIP</a></h2>
</body>
</html>